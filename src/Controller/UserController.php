<?php
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager as DocumentManager;


class UserController extends AbstractController
{
  public function index(DocumentManager $dm) : Response{
    $user = $dm->getRepository(User::class)->findAll();
    return $this->render('base.html.twig', $user);
  }

  public function login(Request $request, AuthenticationUtils $authenticationUtils) : Response{
    $errors = $authenticationUtils->getLastAuthenticationError();
    $lastUsername = $authenticationUtils->getLastUsername();
    return $this->render('user_login.html.twig', [
      'errors'    =>  $errors,
      'username'  =>  $lastUsername
    ]);
  }

  public function logout() : Response{

  }

  public function Register() : Response {
    return $this->render('user_register.html.twig');
  }
}