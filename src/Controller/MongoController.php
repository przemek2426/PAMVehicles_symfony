<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager as DocumentManager;

class MongoController extends AbstractController
{
  /**
   * @Route("/mongoTest", methods={"GET"})
   */
  public function mongoTest(DocumentManager $dm)
  {
    $user = new User();
    $user->setEmail("przemek2426@gmail.com");
    $user->setUsername("przemek");
    $user->setPassword("1234");
    $user->setFirstName("Przemysław");
    $user->setSurname("Kulig");
    $user->setCity("Opole");
    $user->setStreet("Fajna");
    $user->setStreetNumber(5);
    $user->setApartmentNumber(2);

    $dm->persist($user);
    $dm->flush();
    return new JsonResponse(array('Status' => 'OK', 'Id' => $user->getId()));
  }
}