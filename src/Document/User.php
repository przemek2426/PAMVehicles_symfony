<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Security\Core\User\UserInterface;
/**
 * @MongoDB\Document
 */
class User implements UserInterface
{
  /**
   * @MongoDB\Id
   */
  protected $id;

  /**
   * @MongoDB\Field(type="string")
   */
  protected $username;

  /**
   * @MongoDB\Field(type="string")
   */
  protected $email;

  /**
   * @MongoDB\Field(type="string")
   */
  protected $password;

  /**
   * @MongoDB\Field(type="string")
   */
  protected $firstName;

  /**
   * @MongoDB\Field(type="string")
   */
  protected $surname;

  /**
   * @MongoDB\Field(type="string")
   */
  protected $city;

  /**
   * @MongoDB\Field(type="string")
   */
  protected $street;

  /**
   * @MongoDB\Field(type="int")
   */
  protected $streetNumber;

  /**
   * @MongoDB\Field(type="int")
   */
  protected $apartmentNumber;

  public function getId(){ return $this->id; }
  public function setId($id){ $this->id = $id;}

  public function getUsername(){ return $this->username; }
  public function setUsername($username){ $this->username = $username; }

  public function getEmail(){ return $this->email; }
  public function setEmail($email){ $this->email = $email; }

  public function getPassword(){ return $this->password; }
  public function setPassword($password){ $this->password = $password; }

  public function getFirstName() { return $this->firstName; }
  public function setFirstName($firstName) { $this->firstName = $firstName; }

  public function getSurname() {  return $this->surname; }
  public function setSurname($surname) { $this->surname = $surname; }

  public function getCity() { return $this->city; }
  public function setCity($city) { $this->city = $city; }

  public function getStreet() { return $this->street; }
  public function setStreet($street) { $this->street = $street; }

  public function getStreetNumber() { return $this->streetNumber; }
  public function setStreetNumber($streetNumber) { $this->streetNumber = $streetNumber; }

  public function getApartmentNumber() { return $this->apartmentNumber; }
  public function setApartmentNumber($apartmentNumber) { $this->apartmentNumber = $apartmentNumber; }

  public function getRoles()
  {
    return ['ROLE_USER'];
  }

  public function getSalt()
  {
    return null;
  }

  public function eraseCredentials()
  {
    return null;
  }
}